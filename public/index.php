<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
      $messages = array();
      // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
      // Выдаем сообщение об успешном сохранении.
      if (!empty($_COOKIE['save'])) {
          // Удаляем куку, указывая время устаревания в прошлом.
          setcookie('save', '', 100000);
          // Если есть параметр save, то выводим сообщение пользователю.
          $messages['OK'] = 'Спасибо, результаты сохранены.';
      }

      // Складываем признак ошибок в массив.
      $errors = array();
      $errors['fio'] = !empty($_COOKIE['fio_error']);
      $errors['email'] = !empty($_COOKIE['email_error']);
      $errors['floor'] = !empty($_COOKIE['floor_error']);
      $errors['limbs'] = !empty($_COOKIE['limbs_error']);
      $errors['birthdate'] = !empty($_COOKIE['birthdate_error']);
      $errors['biography'] = !empty($_COOKIE['biography_error']);
      $errors['agree'] = !empty($_COOKIE['agree_error']);
      $errors['superskills'] = !empty($_COOKIE['superskills_error']);


if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.

    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages['fio'] = '<div class="error">Заполните имя.</div>';
}
if ($errors['email']){

    setcookie('email_error', '', 100000);
    $messages['email'] = '<div class="error">Заполните email  в формате abcd@abcd.abcd.</div>';
}

if ($errors['floor']){
    $messages['floor'] = '<div class="error">Заполните пол</div>';
    setcookie('floor_error', '', 100000);
}
if ($errors['limbs']){
    $messages['limbs'] = '<div class="error">Заполните кол-во конечностей</div>';
    setcookie('limbs_error', '', 100000);
}
if ($errors['birthdate']){
    $messages['birthdate'] = '<div class="error">Заполните дату рождения</div>';
    setcookie('birthdate_error', '', 100000);
}
if ($errors['biography']){
    $messages['biography'] = '<div class="error">Заполните биографию</div>';
    setcookie('biography_error', '', 100000);
}
if ($errors['agree']){
    $messages['agree'] = '<div class="error">Заполните согласие</div>';
    setcookie('agree_error', '', 100000);
}
if ($errors['superskills']){
        $messages['superskills'] = '<div class="error">Заполните суперспособности</div>';
        setcookie('suoerskills_error', '', 100000);
}

// Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['floor'] = empty($_COOKIE['floor_value']) ? '' : $_COOKIE['floor_value'];
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['birthdate'] = empty($_COOKIE['birthdate_value']) ? '' : $_COOKIE['birthdate_value'];
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    $values['agree'] = empty($_COOKIE['agree_value']) ? '' : $_COOKIE['agree_value'];
    $values['superskills'] = empty($_COOKIE['superskills']) ? '' : unserialize($_COOKIE['superskills']);

    // TODO: аналогично все поля.

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле email.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['floor'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('floor_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('floor_value', $_POST['floor'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['superskills'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('superskills_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('superskills', serialize($_POST['superskills']), time() + 30 * 24 * 60 * 60);
        }
    if (empty($_POST['limbs'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['birthdate'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('birthdate_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('birthdate_value', $_POST['birthdate'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['biography'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('biography_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['agree'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('agree_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('agree_value', $_POST['agree'], time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('floor_error', '', 100000);
        setcookie('birthdate_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('agree_error', '', 100000);
    }

// Сохранение в базу данных.
//print_r($_POST);
    $user = 'u21044';
    $pass = '76898768';
    try {
        $db = new PDO('mysql:host=localhost;dbname=u21044', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
// Подготовленный запрос. Не именованные метки.
        $name = $_POST["fio"];
        $mail = $_POST["email"];
        $birthdate = $_POST["birthdate"];
        $sex = $_POST["floor"];
        $limbs = $_POST["limbs"];
        $biography = $_POST["biography"];
        $sql = "INSERT into application set name = ?, mail = ?, birthdate = ?, sex = ?, limbs = ?, biography = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute([$name, $mail, $birthdate, $sex, $limbs, $biography]);
        $db->exec($sql);
        $last_id = $db->lastInsertId();

        $sql = "INSERT into app_sup set id = ?, sup_id =?";
        $stmt = $db->prepare($sql);
        foreach ($_POST["superskills"] as $val) {
            $stmt->execute([$last_id, $val]);
        }
    }//
    catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1', time()+100);
    header('Location: ?save=1');
}
